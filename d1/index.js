/*
	-use the "require" directive to load the expess module/package
	-a "module " is a software component or part of a program that contains one or more routines
	-this is used to get the contents of the package to be used by our application
	-it also allows us to access methods and function that will allow us to easilky create server
*/

const express = require("express");
/*
	-create an application using express
	-this creates an express application and stores this is a constant called app
	-in layman's term, app is our server
*/
const app = express()
//for our appication server to run we need a port to listen to
const port = 3000;


/*MIDDLEWARES
	-SET up for allowing th server to handle data from requests
	-allows your app to read json data
	-methods used from express.js are middlewares
	-middleware is a layes of software thjat enable to interaction and transmission of information between assorted application
*/
app.use(express.json());


/*
	-allows your aoo to read data from forms
	-by defaults , information received from the url can only be received as string or an array
	-by applying hte option of "extended:true", we are allowed to receive information inother data types such as an object throughout our application.
*/
app.use(express.urlencoded({extended: true}));

// ROUTES
/*
	-Express has methods corresponding to each HTTP method
	-thisroute expects to receive a GET request at the base URI
	-the full base URL for our local application for the route will be at 
	"http://localhost:3000"
*/

// Returns simple messges
/*
	-this route expects to revive to get request at the base URI"/"

	POSTMAN
	url:http://localhost:3000/
	method: GET
*/
app.get('/',(request, response) =>{
	response.send('Hello World')
});

// REturns simple message
/*
	URI:/Hello
	Method: "GET"


	POSTMAN
	url: http://localhost:3000/hello
	get
*/
app.get('/hello',(request, response) =>{
	response.send('Hello from "/Hello endpoint')
});

/*
		Return simple GREETING

	URI:/Hello
	Method: "POST"


	POSTMAN
	url: http://localhost:3000/hello
	MEthod : POST\
	Body: raw +json
	{
		"firstName" : "Ralph",
		"lastName" : "Dagasdas"

	}

*/
app.post('/hello',(request, response) =>{
	response.send(`Hello there, ${request.body.firstName} ${request.body.lastName}! this is from the "/hello" endpoint but w/ a post method`)
});


// Register user route
/*
	-this route expects to reveive apost request at th URI "/register"
	-this will create a user object in the "users" var that mirrors a real world registration process

	URI: /hello
	method: POST

	POSTMAN
	URL : http://localhost:3000/register
	Method: 'POST'
	body: raw +json
	{
		"username": "ralph",
		"password": "123"

	}

*/

let users = [];

app.post('/register',(request, response)=>{

	if(request.body.username !=='' && request.body.password !== ''){
		users.push(request.body);
		response.send(`User ${request.body.username} successfully registered`)
		console.log(request.body)
	}else{
		response.send('please inboth username and password')
	}

})

// change password route
/*
	this route expects to receive A PUT request at the URI "/change-password"
	this will update the password of a user that matches th info provided ib the cliecnt/ postman

	URI: /change-password
	method : PUT

	POSTMAN
	URL : http://localhost:3000/change-password
	methd :PUT

	body: raw +json
	{
		"username": "ralph",
		"password": "123"

	}

*/

app.use("/change-password", (request, response) =>{
	// creates a var to store the message to be sent back to the cleint/POSTMAN
	let message;

	// creates a for loop that will loop through the elements of the "users array"
	for(let i=0; i<users.length; i++){
		// if the username provided in the client /postman and the username of the current object in the loop is the same
		if(request.body.username == users[i].username){
			// changes the password of the user found by the loop into a password provided in th cleint/postman
			users[i].password =request.body.password
			// changes the message to be sent if the password has been updated
			message = `User ${request.body.username}'s password has been updated`
			// breaks out of the loop ouce user that matchs the user
			break;

		}else{
			message = "User does not Exist"
		}
	}
	response.send(message)
})

/*
	-tells our server to listen to the port
	-if the port is accessed, we run the server
	-Returns a message to confirm that the server is running in the terminal
*/ 
app.listen(port, () => console.log(`Server running at port ${port}`))